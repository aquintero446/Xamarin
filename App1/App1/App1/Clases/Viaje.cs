﻿using System;
using SQLite;

namespace App1.Clases
{
    public class Viaje
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [MaxLength(150)]
        public string Nombre { get; set; }
        public DateTime FechaIda { get; set; }
        public DateTime FechaRegreso { get; set; }
        /*
         // En caso de mala visualizacion de la lista
        public override string ToString()
        {
            return $"{Nombre} ({FechaIda:d} - {FechaRegreso:d})";
        }
        */
    }

}
