﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App1.Clases
{
    class InicioSesion
    {
        public static bool IniciarSesion(string usuario, string password)
        {
            if (string.IsNullOrEmpty(usuario) || string.IsNullOrEmpty(password))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
