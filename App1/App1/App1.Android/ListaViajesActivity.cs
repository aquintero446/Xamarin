﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App1.Clases;

namespace App1.Droid
{
    [Activity(Label = "ListaViajesActivity")]
    public class ListaViajesActivity : Activity
    {
        List<Viaje> viajes;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            CargarViajes();
        }
        protected override void OnRestart()
        {
            base.OnRestart();
            CargarViajes();
        }

        //Codigo encapsulado aporte de matiasniz
        private void CargarViajes()
        {
            string nombreArchivo = "viajes_db.sqlite";
            string rutaCarpeta = System.Environment.GetFolderPath((System.Environment.SpecialFolder.Personal));
            string ruta = Path.Combine(rutaCarpeta, nombreArchivo);

            viajes = new List<Viaje>();
            viajes = DatabaseHelper.LeerViajes(ruta);

            var arrayAdapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleListItem1, viajes);
            ListAdapter = arrayAdapter;
        }
    }
}