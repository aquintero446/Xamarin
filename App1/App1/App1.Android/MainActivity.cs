﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using App1.Clases;
using Android.Content;

namespace App1.Droid
{
    [Activity(Label = "App1", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        EditText nombreUsuarioEditText, passwordEditText;
        Button inicioSesionButton;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            nombreUsuarioEditText = FindViewById<EditText>(Resource.Id.usuarioEditText);
            passwordEditText = FindViewById<EditText>(Resource.Id.PasswordEditText);
            inicioSesionButton = FindViewById<Button>(Resource.Id.inicioSesionButton);

            inicioSesionButton.Click += InicioSesionButton_Clicked;
        }
        private void InicioSesionButton_Clicked(object sender, EventArgs argumentos)
        {
            if(InicioSesion.IniciarSesion(nombreUsuarioEditText.Text, passwordEditText.Text))
            {
                //TODO: Navegar
                Intent intent = new Intent(this, typeof(ListaViajesActivity));
                StartActivity(intent);
            }
        }
    }
}

